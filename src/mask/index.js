Component({
  properties: {
    show: {
      type: Boolean,
      value: false,
    },
    opacity: {
      type: Number,
      optionalTypes: [String],
      value: 0.3,
    },
    locked: {
      type: Boolean,
      value: false,
    },
    center: {
      type: Boolean,
      value: false,
    },
    zIndex: {
      type: Number,
      optionalTypes: [String],
      value: 60,
    },
  },

  methods: {
    // 阻止事件页面滑动
    onMaksMove() {},

    onMask() {
      if (!this.data.locked) {
        this.setData({
          show: false,
        })
      }
    },
  },
})
