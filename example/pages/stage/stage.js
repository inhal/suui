// pages/stage/stage.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    showDrawer: false
  },

  onShowDraw() {
    this.setData({
      showDrawer: true
    })
  }
})
