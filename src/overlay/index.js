Component({
  properties: {
    show: {
      type: Boolean,
      value: false,
    },
    placement: {
      // 弹出层方向
      // top bottom left right center
      type: String,
      value: 'bottom',
    },
    zIndex: {
      type: Number,
      value: 100,
    },
    locked: Boolean,
  },

  data: {
    placementType: ['top', 'bottom', 'left', 'right', 'center'],
  },

  methods: {
    onOverlay() {
      this.triggerEvent('click-overlay', {}, {})
      if (!this.data.locked) {
        this.setData({
          show: false,
        })
      }
    },
    // 阻止事件页面滑动
    onOverlayMove() {},
  },
})
