const { series, parallel } = require('gulp')
const watch = require('gulp-watch')
const path = require('path')

const { buildWxss, copyFiles, clean, copy } = require('./build')

const SRC_PATH = path.resolve(__dirname, '../src')
const DIST_PATH = path.resolve(__dirname, '../dist')
const EXAMPLE_PATH = path.resolve(__dirname, '../example/dist')

module.exports = {
  dev: series(
    clean(EXAMPLE_PATH),
    parallel(
      buildWxss(SRC_PATH, EXAMPLE_PATH),
      copyFiles(SRC_PATH, EXAMPLE_PATH)
    )
  ),

  build: series(
    clean(DIST_PATH),
    parallel(buildWxss(SRC_PATH, DIST_PATH), copyFiles(SRC_PATH, DIST_PATH))
  ),

  watch: parallel(() => {
    watch(`${SRC_PATH}/**/*.js`, copy(SRC_PATH, EXAMPLE_PATH, 'js'))
    watch(`${SRC_PATH}/**/*.json`, copy(SRC_PATH, EXAMPLE_PATH, 'json'))
    watch(`${SRC_PATH}/**/*.wxml`, copy(SRC_PATH, EXAMPLE_PATH, 'wxml'))
    watch(`${SRC_PATH}/**/*.less`, buildWxss(SRC_PATH, EXAMPLE_PATH))
  }),
}
