Component({
  properties: {
    block: {
      type: Boolean,
      value: false,
    },
    type: {
      type: String,
      value: 'primary', // primary success warning danger
    },
    size: {
      type: String,
      value: 'normal', // small normal large
    },
    flat: {
      type: Boolean,
      value: false,
    },
    width: {
      type: Number,
      value: 0,
    },
    disabled: {
      type: Boolean,
      value: false,
    },
    loading: {
      type: Boolean,
      value: false,
    },
    openType: {
      type: String,
      value: '',
    },
    appParameter: String,
    sessionFrom: String,
    sendMessageTitle: String,
    sendMessagePath: String,
    sendMessageImg: String,
    showMessageCard: Boolean,
  },

  methods: {
    onOpenTypeEvent(e) {
      console.log(e)
      this.triggerEvent(e.type, e.detail, {})
    },
  },
})
